# Social.coop Policies

[Social.coop](https://social.coop/) policies as a [Gitbook](http://gitbook.com/), hosted at [https://loppear.gitlab.io/social-coop-policies/](https://loppear.gitlab.io/social-coop-policies/)

See the <---- menu for the policies.

## Contributing

Policies in draft form should be linked in SUMMARY.md under the Drafts heading.

Accepted policies should be linked in SUMMARY.md under the Accepted heading, and include an arbitrary version number and date of acceptance.

Proposed changes to either type of policy document may be submitted as separate documents with a version/author indicator, or as a diff to the existing document. For the latter,  please fork this [respository](https://loppear.gitlab.io/social-coop-policies/) and edit any documents you wish, submitting a Merge Request (Pull Request) with changes.

As drafts are circulated on the official Loomio site outside of version control, I hope to keep this repository up to date.

## Technical Notes

Repository based on [Gitlab's pages support for Gitbook](https://gitlab.com/pages/gitbook)
